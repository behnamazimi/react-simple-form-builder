import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import __ from "../constants/localization";

export const CheckboxGroup = ({element, attributes, onObjectChange, editMode, local}) => {

    const uniqueID = 'checkbox-' + Math.floor(Math.random() * 1000000 + 10000)
    let elementOptions = element.options

    const addOptionToGroup = () => {
        elementOptions.push({
            selected: false,
            label: '',
            value: '',
        })

        onObjectChange('options', elementOptions)

    }

    const removeListOption = (key) => {
        delete elementOptions[key]
        onObjectChange('options', elementOptions.filter(String))

    }

    const handleOptionDetailsChange = (key, type, value) => {

        elementOptions[key][type] = value
        onObjectChange('options', elementOptions)

    }

    return (<React.Fragment>
        <div className="d-flex">
            {element.label &&
            <label htmlFor={uniqueID}>{element.label + (element.required ? ' *' : '')}:&nbsp;</label>}

            <div className={classNames('f-el', 'checkbox-group', element.type)}>
                {elementOptions.map((option, key) => (
                    <div key={key}>
                        <label htmlFor={uniqueID + '-' + key}>{option.label}</label>
                        <input checked={option.selected} type="checkbox" value={option.value} id={uniqueID + '-' + key}
                               name={uniqueID}
                               onChange={() => handleOptionDetailsChange(key, 'selected', !option.selected)}/>
                    </div>

                ))}
            </div>
        </div>

        <div className={classNames("edit-panel", editMode && 'edit-mode')}>
            <div className="edit-inner">
                <div className="edit-row">
                    <label htmlFor="required-label">{__('Required', local)}:&nbsp;</label>
                    <input type="checkbox" id="required-label" value={element.required}
                           onChange={(e) => onObjectChange('required', !element.required)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-label">{__('Label', local)}:&nbsp;</label>
                    <input type="text" id="title-label" value={element.label} maxLength={400}
                           onChange={(e) => onObjectChange('label', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="maxLength-label">{__('Options', local)}:&nbsp;</label>
                    <div className="options">
                        {elementOptions.map((option, key) => (
                            <div className="option" key={key}>
                                <input type="checkbox" checked={option.selected} name={'edit-' + uniqueID + '-' + key}
                                       onChange={() => handleOptionDetailsChange(key, 'selected', !option.selected)}/>
                                <input value={option.label} placeholder="Label"
                                       onChange={(e) => handleOptionDetailsChange(key, 'label', e.target.value)}/>
                                <input value={option.value || option.label} placeholder={__('Placeholder', local)}
                                       onChange={(e) => handleOptionDetailsChange(key, 'value', e.target.value)}/>
                                <button className="remove-option"
                                        onClick={() => removeListOption(key)}>{__('Remove', local)}</button>
                            </div>
                        ))}
                        <button className="add-option" onClick={addOptionToGroup}>{__('Add', local)}</button>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>)
}


CheckboxGroup.propTypes = {
    onObjectChange: PropTypes.func,
    element: PropTypes.object,
    attributes: PropTypes.object,
    editMode: PropTypes.bool,
}
CheckboxGroup.defaultProps = {
    element: {},
    editMode: false,
}