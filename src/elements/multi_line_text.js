import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import __ from "../constants/localization";

export const MultiLineText = ({element, attributes, onObjectChange, editMode, local}) => {

    const uniqueID = Math.floor(Math.random() * 1000000 + 10000)

    return (<React.Fragment>
        <div className="d-flex">
            {element.label &&
            <label
                htmlFor={element.type + '_' + uniqueID}>{element.label + (element.required ? ' *' : '')}:&nbsp;</label>}

            <textarea className={classNames('f-el', 'multi-line-text', element.type)}
                      placeholder={element.placeholder} maxLength={element.maxLength} rows={element.rows}
                      style={{textAlign: element.align}} value={element.value}
                      onChange={(e) => onObjectChange('value', e.target.value)}/>
        </div>
        <div className={classNames("edit-panel", editMode && 'edit-mode')}>
            <div className="edit-inner">
                <div className="edit-row">
                    <label htmlFor="required-label">{__('Required', local)}:&nbsp;</label>
                    <input type="checkbox" id="required-label" value={element.required}
                           onChange={(e) => onObjectChange('required', !element.required)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-label">{__('Label', local)}:&nbsp;</label>
                    <input type="text" id="title-label" value={element.label} maxLength={400}
                           onChange={(e) => onObjectChange('label', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="placeholder-label">{__('Placeholder', local)}:&nbsp;</label>
                    <input id="placeholder-label" value={element.placeholder} maxLength={100}
                           onChange={(e) => onObjectChange('placeholder', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="value-label">{__('Value', local)}:&nbsp;</label>
                    <textarea id="value-label" value={element.value} maxLength={255}
                              onChange={(e) => onObjectChange('value', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="rows-label">{__('Rows', local)}:&nbsp;</label>
                    <input id="rows-label" value={element.rows} type="number"
                           onChange={(e) => onObjectChange('rows', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="maxLength-label">{__('Max Length', local)}:&nbsp;</label>
                    <input id="maxLength-label" value={element.maxLength} type="number"
                           onChange={(e) => onObjectChange('maxLength', e.target.value)}/>
                </div>
            </div>
        </div>
    </React.Fragment>)
}


MultiLineText.propTypes = {
    onObjectChange: PropTypes.func,
    element: PropTypes.object,
    attributes: PropTypes.object,
    editMode: PropTypes.bool,
}
MultiLineText.defaultProps = {
    element: {},
    editMode: false,
}