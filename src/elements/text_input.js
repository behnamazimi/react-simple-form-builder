import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import __ from "../constants/localization";

export const TextInput = ({element, attributes, onObjectChange, editMode, local}) => {

    const uniqueID = Math.floor(Math.random() * 1000000 + 10000)

    return (<React.Fragment>
        <div className="d-flex">
            {element.label &&
            <label
                htmlFor={element.type + '_' + uniqueID}>{element.label + (element.required ? ' *' : '')}:&nbsp;</label>}

            <input className={classNames('f-el', 'text-input', element.type)} type={element.type}
                   placeholder={element.placeholder}
                   style={{textAlign: element.align}} value={element.value}
                   onChange={(e) => onObjectChange('value', e.target.value)}/>
        </div>

        <div className={classNames("edit-panel", editMode && 'edit-mode')}>
            <div className="edit-inner">
                <div className="edit-row">
                    <label htmlFor="required-label">{__('Required', local)}:&nbsp;</label>
                    <input type="checkbox" id="required-label" value={element.required}
                           onChange={(e) => onObjectChange('required', !element.required)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-label">{__('Label', local)}:&nbsp;</label>
                    <input type="text" id="title-label" value={element.label} maxLength={400}
                           onChange={(e) => onObjectChange('label', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="placeholder-label">{__('Placeholder', local)}:&nbsp;</label>
                    <input id="placeholder-label" value={element.placeholder} maxLength={100}
                           onChange={(e) => onObjectChange('placeholder', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="value-label">{__('Value', local)}:&nbsp;</label>
                    <input id="value-label" value={element.value} maxLength={255} type={element.type}
                           onChange={(e) => onObjectChange('value', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-type">{__('Type', local)}:&nbsp;</label>
                    <select id="title-type" value={element.type}
                            onChange={(e) => onObjectChange('type', e.target.value)}>
                        {attributes.types.map((item, key) => (
                            <option key={key} value={item}>{__(item, local)}</option>
                        ))}
                    </select>
                </div>
                <div className="edit-row">
                    <label htmlFor="maxLength-label">{__('Max Length', local)}:&nbsp;</label>
                    <input id="maxLength-label" value={element.maxLength} type="number"
                           onChange={(e) => onObjectChange('maxLength', e.target.value)}/>
                </div>
            </div>
        </div>
    </React.Fragment>)
}


TextInput.propTypes = {
    onObjectChange: PropTypes.func,
    element: PropTypes.object,
    attributes: PropTypes.object,
    editMode: PropTypes.bool,
}
TextInput.defaultProps = {
    element: {},
    editMode: false,
}