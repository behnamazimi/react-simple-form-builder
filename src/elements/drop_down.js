import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import __ from "../constants/localization";

export const DropDown = ({element, attributes, onObjectChange, editMode, local}) => {

    const uniqueID = Math.floor(Math.random() * 1000000 + 10000)
    let elementOptions = element.options

    const addOptionToList = () => {
        elementOptions.push({
            selected: false,
            label: '',
            value: '',
        })

        onObjectChange('options', elementOptions)

    }

    const removeListOption = (key) => {
        delete elementOptions[key]
        onObjectChange('options', elementOptions.filter(String))

    }

    const handleOptionSelect = (key, status) => {
        elementOptions[key].selected = status
        onObjectChange('options', elementOptions)

    }

    const handleOptionDetailsChange = (key, type, value) => {
        elementOptions[key][type] = value
        onObjectChange('options', elementOptions)

    }

    let selectedOption = ''

    return (<React.Fragment>
        <div className="d-flex">
            {element.label &&
            <label
                htmlFor={element.type + '_' + uniqueID}>{element.label + (element.required ? ' *' : '')}:&nbsp;</label>}

            <select className={classNames('f-el', 'drop-down', element.type)}
                    placeholder={element.placeholder} multiple={element.multiple}
                    style={{textAlign: element.align}} defaultValue={selectedOption}
                    onChange={(e) => onObjectChange('value', e.target.value)}>
                {elementOptions.map((option, key) => {
                    if (option.selected)
                        selectedOption = option.value

                    return (
                        <option value={option.value || option.label}
                                key={key}>{option.label}</option>
                    )
                })}
            </select>
        </div>

        <div className={classNames("edit-panel", editMode && 'edit-mode')}>
            <div className="edit-inner">
                <div className="edit-row">
                    <label htmlFor="required-label">{__('Required', local)}:&nbsp;</label>
                    <input type="checkbox" id="required-label" value={element.required}
                           onChange={(e) => onObjectChange('required', !element.required)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="multiple-label">{__('Multiple', local)}:&nbsp;</label>
                    <input type="checkbox" id="multiple-label" value={element.multiple}
                           onChange={(e) => onObjectChange('multiple', !element.multiple)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-label">{__('Label', local)}:&nbsp;</label>
                    <input type="text" id="title-label" value={element.label} maxLength={400}
                           onChange={(e) => onObjectChange('label', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="placeholder-label">{__('Placeholder', local)}:&nbsp;</label>
                    <input id="placeholder-label" value={element.placeholder} maxLength={100}
                           onChange={(e) => onObjectChange('placeholder', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="maxLength-label">{__('Options', local)}:&nbsp;</label>
                    <div className="options">
                        {elementOptions.map((option, key) => (
                            <div className="option" key={key}>
                                <input type="checkbox" checked={option.selected}
                                       onChange={() => handleOptionSelect(key, !option.selected)}/>
                                <input value={option.label} placeholder="Label"
                                       onChange={(e) => handleOptionDetailsChange(key, 'label', e.target.value)}/>
                                <input value={option.value || option.label} placeholder="Value"
                                       onChange={(e) => handleOptionDetailsChange(key, 'value', e.target.value)}/>
                                <button className="remove-option"
                                        onClick={() => removeListOption(key)}>{__('Remove', local)}</button>
                            </div>
                        ))}
                        <button className="add-option" onClick={addOptionToList}>{__('Add', local)}</button>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>)
}

DropDown.propTypes = {
    onObjectChange: PropTypes.func,
    element: PropTypes.object,
    attributes: PropTypes.object,
    editMode: PropTypes.bool,
}
DropDown.defaultProps = {
    element: {},
    editMode: false,
}