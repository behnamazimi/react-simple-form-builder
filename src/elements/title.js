import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import __ from "../constants/localization";

export const Title = ({element, attributes, onObjectChange, editMode, local}) => {

    const Type = element.type

    return (<React.Fragment>
        <Type className={classNames('f-el', 'title', element.type)}
              style={{textAlign: element.align}}>{element.label}</Type>
        <div className={classNames("edit-panel", editMode && 'edit-mode')}>
            <div className="edit-inner">
                <div className="edit-row">
                    <label htmlFor="title-label">{__('Label', local)}:&nbsp;</label>
                    <input type="text" id="title-label" value={element.label} maxLength={255}
                           onChange={(e) => onObjectChange('label', e.target.value)}/>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-align">{__('Align', local)}:&nbsp;</label>
                    <select id="title-align" value={element.align}
                            onChange={(e) => onObjectChange('align', e.target.value)}>
                        {attributes.aligns.map((item, key) => (
                            <option key={key} value={item}>{__(item[0].toUpperCase() + item.slice(1), local)}</option>
                        ))}
                    </select>
                </div>
                <div className="edit-row">
                    <label htmlFor="title-type">{__('Type', local)}:&nbsp;</label>
                    <select id="title-type" value={element.type}
                            onChange={(e) => onObjectChange('type', e.target.value)}>
                        {attributes.types.map((item, key) => (
                            <option key={key} value={item}>{item}</option>
                        ))}
                    </select>
                </div>
            </div>
        </div>
    </React.Fragment>)
}

Title.propTypes = {
    onObjectChange: PropTypes.func,
    element: PropTypes.object,
    attributes: PropTypes.object,
    editMode: PropTypes.bool,
}
Title.defaultProps = {
    element: {},
    editMode: false,
}