import React from 'react';
import ReactDOM from 'react-dom';
import RSFormBuilder from './RSFormBuilder';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<RSFormBuilder local='fa' full={true} onFormChange={els=>console.log(els)}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
