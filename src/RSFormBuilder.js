import React, {Component} from 'react';
import {DragDropContext} from "react-dnd";
import HTML5Backend from 'react-dnd-html5-backend'

import Panel from "./Panel";
import FormTool from "./FormToolbar";
import "./styles/index.scss"
import tools from "./constants/tools";
import classNames from "classnames"
import __ from "./constants/localization";
import update from "immutability-helper"
import global from "./constants/global";

class RSFormBuilder extends Component {

    constructor(props) {
        super(props)

        this.state = {
            formElements: []
        }

    }

    handleElementsUpdate = (newElements) => {

        this.setState({
            formElements: Array.from(newElements)
        })

        this.props.onFormChange(this.state.formElements.map(el => el.obj))
    }

    handleElementsSort = (dragIndex, hoverIndex) => {

        const {formElements} = this.state
        const dragCard = formElements[dragIndex]

        this.setState(
            update(this.state, {
                formElements: {
                    $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
                },
            }),
        )

        this.props.onFormChange(this.state.formElements.map(el => el.obj))
    }


    handleElementRemove = (toolIndex) => {

        this.setState(
            update(this.state, {
                formElements: {
                    $splice: [[toolIndex, 1]]
                }
            }))

        this.props.onFormChange(this.state.formElements.map(el => el.obj))
    }

    handleElementUpdate = (index, obj) => {
        let elements = this.state.formElements

        elements[index].obj = obj

        this.setState({
            formElements: elements,
        })

        this.props.onFormChange(this.state.formElements.map(el => el.obj))
    }

    addToolToPanelOnClick = (newToolID) => {

        let newTool = tools.filter(tool => tool.id === newToolID)[0]

        newTool = update(newTool, {
            key: {$set: newTool.id + '-' + ((new Date()).getTime().toString())}
        })

        this.setState(update(this.state, {
            formElements: {$push: [newTool]}
        }))
    }

    render() {
        const {full, style, local, className} = this.props

        const {selectedLocal} = this.state

        const currentLocal = selectedLocal || (local || 'en')


        const Tools = tools.map((tool, key) => (
            <FormTool key={key} id={tool.id} name={__(tool.name, currentLocal)}
                      onToolClick={this.addToolToPanelOnClick}/>
        ))

        return (
            <div className={classNames('RSFormBuilder', currentLocal, className, full ? 'full' : '')} style={style}>
                <aside className="toolbar">
                    <h2 className="title">{global.libName}</h2>
                    {Tools}
                    <div className="details">{global.version}</div>
                </aside>
                <section className="inner">
                    <div className="description">
                        {'Fill your form with available fields please.'}
                    </div>
                    <div className="main-actions">
                        <button className="action-btn" onClick={() => this.setState({formElements: []})}>Reset</button>

                        <label htmlFor="local-select">
                            <span>{__('Local', currentLocal)}:&nbsp;</span>
                            <select value={currentLocal} id="local-select"
                                    onChange={(e) => this.setState({selectedLocal: e.target.value})}>
                                <option value="en">EN</option>
                                <option value="fa">FA</option>
                            </select>
                        </label>
                    </div>
                    <Panel className="panel" local={currentLocal}
                           formElements={this.state.formElements}
                           onElementsSortChange={this.handleElementsSort}
                           onElementRemove={this.handleElementRemove}
                           onElementUpdate={this.handleElementUpdate}
                           updateElementsTo={this.handleElementsUpdate}/>

                </section>

            </div>
        );
    }
}


export default DragDropContext(HTML5Backend)(RSFormBuilder);
