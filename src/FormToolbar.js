import * as React from 'react'
import {DragSource, ConnectDragSource} from 'react-dnd'
import tools from "./constants/tools";

const boxSource = {
    beginDrag(props, monitor) {

        const dragTool = tools.filter(tool => tool.id === props.id)[0]

        return {
            item: dragTool
        }
    },
}

class FormToolbar extends React.Component {
    render() {
        const {isDragging, connectDragSource, name, onToolClick, id} = this.props

        return connectDragSource(
            <div className="FormToolbar" onClick={() => onToolClick(id)}>
                {name}
            </div>,
            'copy',
        )
    }
}

export default DragSource(
    'FBTool',
    boxSource,
    (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging(),
    }),
)(FormToolbar)