const __ = (trend, local) => {

    // return trend if local not pass.
    if (!local || local == null)
        return trend

    const lcTrend = trend.toString().toLowerCase()

    if (
        !localization[lcTrend] // if trend not exist in localization
        || localization[lcTrend] == null // if not exist in any way
        || (localization[lcTrend] && !localization[lcTrend][local]) // if trend exist in localization but not localized yet for the "local"
    )
        return trend

    return localization[lcTrend][local]
}

const localization = {
    'local': {
        fa: 'زبان'
    },
    'header': {
        fa: 'عنوان'
    },
    'reset': {
        fa: 'پاک کردن همه'
    },
    'remove': {
        fa: 'حذف'
    },
    'edit': {
        fa: 'ویرایش'
    },
    'add': {
        fa: 'افزودن'
    },
    'required': {
        fa: 'الزامی'
    },
    'label': {
        fa: 'برچسب'
    },
    'value': {
        fa: 'مقدار'
    },
    'rows': {
        fa: 'تعداد سطر'
    },
    'multiple': {
        fa: 'چند تایی'
    },
    'drag item here': {
        fa: 'آیتم را به اینجا بکشید'
    },
    'title': {
        fa: 'عنوان'
    },
    'placeholder': {
        fa: 'متن پیش فرض'
    },
    'max length': {
        fa: 'حداکثر طول'
    },
    'paragraph': {
        fa: 'پاراگراف'
    },
    'text input': {
        fa: 'ورودی متن'
    },
    'multi line text': {
        fa: 'ورودی متن طولانی'
    },
    'list': {
        fa: 'لیست'
    },
    'checkbox': {
        fa: 'چک باکس'
    },
    'radio group': {
        fa: 'گزینه تکی'
    },
    'radio': {
        fa: 'انتخاب'
    },
    'checkbox group': {
        fa: 'گزینه چند تایی'
    },
    'align': {
        fa: 'چینش'
    },
    'right': {
        fa: 'راست'
    },
    'left': {
        fa: 'چپ'
    },
    'justify': {
        fa: 'مرتب'
    },
    'center': {
        fa: 'وسط'
    },
    'options': {
        fa: 'گزینه ها'
    },
    'option': {
        fa: 'گزینه'
    },
    'type': {
        fa: 'نوع'
    },
    'email': {
        fa: 'ایمیل'
    },
    'number': {
        fa: 'عدد'
    },
    'password': {
        fa: 'رمز'
    },
    'text': {
        fa: 'متن'
    },
    'close': {
        fa: 'بستن'
    },
}


export default __