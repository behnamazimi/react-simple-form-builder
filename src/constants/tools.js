import {Title} from "../elements/title";
import {Paragraph} from "../elements/paragraph";
import {RadioGroup} from "../elements/radio_group";
import {CheckboxGroup} from "../elements/checkbox_group";
import {DropDown} from "../elements/drop_down";
import {MultiLineText} from "../elements/multi_line_text";
import {TextInput} from "../elements/text_input";

const tools = [
    {
        name: 'Title',
        attributes: {
            aligns: ['right', 'center', 'left', 'justify'],
            types: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'label']
        },
        obj: {
            label: 'Header',
            align: 'left',
            type: 'h1',
        },
        component: Title,
        id: 'header',
    },
    {
        name: 'Paragraph',
        attributes: {
            aligns: ['right', 'center', 'left', 'justified'],
            types: ['p', 'strong', 'address', 'blockquote']
        },
        obj: {
            label: 'Paragraph',
            align: 'left',
            type: 'p',
        },
        component: Paragraph,
        id: 'paragraph',
    },
    {
        name: 'Text Input',
        attributes: {
            types: ['text', 'email', 'number', 'password']
        },
        obj: {
            required: false,
            label: 'Text Input',
            placeholder: 'Enter Your Text Here',
            value: '',
            maxLength: 255,
            type: 'text'
        },
        component: TextInput,
        id: 'text-input',
    },
    {
        name: 'Multi Line Text',
        attributes: {},
        obj: {
            required: false,
            label: 'Multi Line Text Input',
            placeholder: 'Enter Your Text Here',
            value: '',
            rows: 3,
            maxLength: 255,
            type: 'textarea'
        },
        component: MultiLineText,
        id: 'multi-line-text',
    }, {
        name: 'List',
        attributes: {},
        obj: {
            required: false,
            multiple: false,
            label: 'List',
            placeholder: 'Select your choice',
            type: 'select',
            options: [
                {
                    label: 'option 1',
                    value: 'option-1',
                    selected: false
                },
                {
                    label: 'option 2',
                    value: 'option-2',
                    selected: true
                }
            ]
        },
        component: DropDown,
        id: 'drop-down',
    },
    {
        name: 'Checkbox',
        attributes: {},
        obj: {
            required: false,
            label: 'Checkbox Group',
            type: 'checkbox-group',
            options: [
                {
                    label: 'option 1',
                    value: 'option-1',
                    selected: false
                },
                {
                    label: 'option 2',
                    value: 'option-2',
                    selected: false
                }
            ]
        },
        component: CheckboxGroup,
        id: 'checkbox-group',
    },
    {
        name: 'Radio Group',
        attributes: {},
        obj: {
            required: false,
            label: 'Radio Group',
            type: 'radio-group',
            options: [
                {
                    label: 'option 1',
                    value: 'option-1',
                    selected: false
                },
                {
                    label: 'option 2',
                    value: 'option-2',
                    selected: false
                }
            ]
        },
        component: RadioGroup,
        id: 'radio-group',
    },
]

export default tools