import * as React from 'react'
import {DropTarget, ConnectDropTarget} from 'react-dnd'
import classNames from "classnames"
import ToolTemplate from "./ToolTemplate";
import update from "immutability-helper"
import __ from "./constants/localization";

const boxTarget = {
    drop(props, monitor, component) {

        let dropResult = monitor.getItem();

        let {formElements} = component.state

        dropResult.item = update(dropResult.item, {
            key: {$set: dropResult.item.id + '-' + ((new Date()).getTime().toString())}
        })

        formElements.push(dropResult.item)

        props.updateElementsTo(formElements)

    },
}

class Panel extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            formElements: props.formElements
        }
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.hasOwnProperty('formElements'))
            this.setState({
                formElements: nextProps.formElements
            })
    }

    render() {

        const {formElements} = this.state

        const {
            canDrop, isOver, connectDropTarget, className, local, onElementsSortChange,
            onElementRemove, onElementUpdate
        } = this.props

        const isActive = canDrop && isOver

        return connectDropTarget(
            <div className={classNames('panel', className, local)}>
                {!formElements.length && (isActive ? __('Release to drop', local) : __('Drag item here', local))}
                {formElements.map((el, key) => (
                    <ToolTemplate key={el.key} index={key} local={local}
                                  moveCard={onElementsSortChange}
                                  onElementUpdate={onElementUpdate}
                                  onElementRemove={onElementRemove}
                                  item={el}/>
                ))}
            </div>,
        )
    }
}

export default DropTarget('FBTool', boxTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
}))(Panel)