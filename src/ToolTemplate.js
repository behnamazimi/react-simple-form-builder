import * as React from 'react'
import {DragSource, DropTarget, ConnectDragSource} from 'react-dnd'
import {findDOMNode} from 'react-dom'
import PropTypes from "prop-types"
import classNames from "classnames"
import tools from "./constants/tools";
import __ from "./constants/localization";

class ToolTemplate extends React.Component {

    constructor(props) {
        super(props)

        let element = null
        let ToolElement = null
        let elementAttrs = null

        tools.slice(0).map(tool => {
            if (tool.id === props.item.id) {
                element = Object.assign({}, tool.obj)
                ToolElement = tool.component
                elementAttrs = tool.attributes
            }
        })

        this.state = {
            toolID: props.item.id,
            element,
            ToolElement,
            elementAttrs,
            editMode: false
        }
    }

    handleObjectChange = (key, value) => {
        let el = this.state.element

        el[key] = value

        this.setState({
            element: el
        })

        this.props.onElementUpdate(this.props.index, Object.assign({}, el))
    }


    render() {
        const {element, ToolElement, elementAttrs, editMode} = this.state

        const {
            isDragging, connectDragSource, connectDropTarget,
            style, onElementRemove, index, local
        } = this.props

        const opacity = isDragging ? 0.1 : 1


        return connectDragSource(
            connectDropTarget(
                <div style={{...style, opacity}}
                     className={classNames('ToolTemplate', local, editMode && 'edit-mode')}>
                    <div className="action-bar">
                        <button type="text" className="action-btn remove-btn"
                                onClick={() => onElementRemove(index)}>{__('Remove', local)}
                        </button>
                        <button type="text" className="action-btn edit-btn"
                                onClick={() => this.setState({
                                    editMode: !editMode
                                })}>{editMode ? __('Close', local) : __('Edit', local)}
                        </button>
                    </div>
                    <ToolElement element={element} editMode={editMode} attributes={elementAttrs}
                                 local={local}
                                 onObjectChange={this.handleObjectChange}/>
                </div>),
        )

    }
}

ToolTemplate.propTypes = {
    moveCard: PropTypes.func,
    onElementRemove: PropTypes.func,
    item: PropTypes.object,
}

ToolTemplate.defaultProps = {
    moveCard: (dragIndex, hoverIndex) => {
    },
    item: {}
}


const toolTemplateSource = {
    beginDrag(props) {
        return {
            index: props.index,
        }
    },
}

const toolTemplateTarget = {
    hover(props, monitor, component) {
        if (!component) {
            return null
        }
        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = (findDOMNode(
            component,
        )).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = (clientOffset).y - hoverBoundingRect.top

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return
        }

        // Time to actually perform the action
        props.moveCard(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    },
}

export default DropTarget(
    'FBToolSort',
    toolTemplateTarget,
    (connect) => ({
        connectDropTarget: connect.dropTarget(),
    }),
)(
    DragSource(
        'FBToolSort',
        toolTemplateSource,
        (connect, monitor) => ({
            connectDragSource: connect.dragSource(),
            isDragging: monitor.isDragging(),
        }),
    )(ToolTemplate),
)